
import View.LogInController;
import View.LogInGUI;

/**
 * The type Main.
 */
public class Main {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args)
    {
        new LogInController(new LogInGUI());
    }
}