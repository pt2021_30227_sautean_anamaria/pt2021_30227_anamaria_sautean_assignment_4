package Model;

import java.io.Serializable;
import java.util.Date;

/**
 * The type Order.
 */
public class Order implements Serializable {
    private int orderId;
    private int clientId;
    private Date date;
    private int totalPrice;

    /**
     * Instantiates a new Order.
     *
     * @param orderId  the order id
     * @param clientId the client id
     * @param date     the date
     */
    public Order(int orderId, int clientId, Date date)
    {
        this.orderId=orderId;
        this.clientId=clientId;
        this.date=date;
    }

    /**
     * Gets order id.
     *
     * @return the order id
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * Sets order id.
     *
     * @param orderId the order id
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets client id.
     *
     * @return the client id
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Sets client id.
     *
     * @param clientId the client id
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets total price.
     *
     * @return the total price
     */
    public int getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets total price.
     *
     * @param totalPrice the total price
     */
    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public int hashCode()
    {
        return orderId+clientId;
    }


}
