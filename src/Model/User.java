package Model;

import java.io.Serializable;

/**
 * The type User.
 */
public class User implements Serializable {
    private String name;
    private final String password;
    private String role;
    private int nrTimesOrdered;
    private int id;

    /**
     * Instantiates a new User.
     *
     * @param id       the id
     * @param name     the name
     * @param password the password
     * @param role     the role
     */
    public User(int id,String name, String password, String role)
    {
        this.id=id;
        this.name=name;
        this.password=password;
        this.role=role;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public Object getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Gets nr times ordered.
     *
     * @return the nr times ordered
     */
    public int getNrTimesOrdered() {
        return nrTimesOrdered;
    }

    /**
     * Sets nr times ordered.
     *
     * @param nrTimesOrdered the nr times ordered
     */
    public void setNrTimesOrdered(int nrTimesOrdered) {
        this.nrTimesOrdered = nrTimesOrdered;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }
}
