package BusinessLayer;

import DataLayer.Bill;
import DataLayer.Serialization;
import Model.Order;
import Model.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Delivery service.
 */
public class DeliveryService extends Observable implements IDeliveryService, Serializable {
    private HashMap<Order, ArrayList<MenuItem>> orders = new HashMap<>();
    private List<MenuItem> menuItemList = new ArrayList<>();
    private List<User> users = new ArrayList<>();
    private Serialization serialization = new Serialization();
    private int nrClients;
    private int nrProducts;
    private int nrOrders;

    /**
     * Add user boolean.
     *
     * @param username the username
     * @param password the password
     * @param role     the role
     * @return the boolean
     * @precondition Check if there are any corrupted data in the structures before the operation
     * @postcondition Check if there are any corrupted data in the structures after the operation
     */
    public boolean addUser(String username, String password, String role)
    {
        assert wellFormed() : "There is data that may be corrupted before addUser operation";
        assert username.equals("") || password.equals("") || role.equals("") : "Can't work with a user without name,role or password";
        if(users.stream().anyMatch(e -> e.getName().equals(username)))
            return false;
       if(role.equals("Client"))
           nrClients++;
       assert wellFormed() : "There is some corrupted data after addUser operation";
       return users.add(new User(nrClients,username, password, role));

    }

    /**
     * Find user boolean.
     *
     * @param username the username
     * @param password the password
     * @param role     the role
     * @return the boolean
     * @precondition Check if there are any corrupted data in the structures before the operation
     * @postcondition Check if there are any corrupted data in the structures after the operation
     */
    public boolean findUser(String username, String password, String role)
    {
        return users.stream().anyMatch(e -> e.getName().equals(username) && e.getRole().equals(role) && e.getPassword().equals(password));
    }

    /**
     *
     * @param name     the name
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     * @return
     * @precondition Check if there are any corrupted data in the structures before the operation
     * @postcondition Check if there are any corrupted data in the structures after the operation
     */
    @Override
    public boolean addProduct(String name, float rating, int calories, int protein, int fat, int sodium, int price) {
        assert wellFormed() : "There is data that may be corrupted before addProduct operation";
        assert name.equals("") || rating < 0 || calories < 0 || protein < 0 || fat < 0 || sodium < 0 || price <0 : "Can't create a product without all the fields completed";
        List<MenuItem> a = menuItemList.stream().filter(e -> e.getName().equals(name)).collect(Collectors.toList());
        if(a.size() > 0)
            return false;
        nrProducts++;
        BaseProduct baseProduct = new BaseProduct(nrProducts,name,rating,calories,protein,fat,sodium,price);
        menuItemList.add(baseProduct);
        assert wellFormed() : "There is some corrupted data after addProduct operation";
        return true;
    }

    /**
     *
     * @param name     the name
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     * @return
     * @precondition Check if there are any corrupted data in the structures before the operation
     * @postcondition Check if there are any corrupted data in the structures after the operation
     */
    @Override
    public boolean modifyProduct(String name, float rating, int calories, int protein, int fat, int sodium, int price) {
        assert wellFormed() : "There is data that may be corrupted before modifyProduct operation";
        assert name.equals("") || rating < 0 || calories < 0 || protein < 0 || fat < 0 || sodium < 0 || price <0 : "Can't modify a product without all the fields completed";

        List<MenuItem> a = menuItemList.stream().filter(e -> e.getName().equals(name)).collect(Collectors.toList());
        if(a.size() == 0)
            return false;
        a.get(0).setRating(rating);
        a.get(0).setCalories(calories);
        a.get(0).setSodium(sodium);
        a.get(0).setProtein(protein);
        a.get(0).setFat(fat);
        a.get(0).setPrice(price);
        assert wellFormed() : "There is some corrupted data after modifyProduct operation";
        return true;
    }

    /**
     * @precondition Check if there are any corrupted data in the structures before the operation
     * @postcondition Check if there are any corrupted data in the structures after the operation
     * @param name the name
     * @return
     */
    @Override
    public boolean deleteProduct(String name) {
        assert wellFormed() : "There is data that may be corrupted before deleteProduct operation";
        assert name.equals("") : "Can't delete a product without name";
        List<MenuItem> a = menuItemList.stream().filter(e -> e.getName().equals(name)).collect(Collectors.toList());
        int size = menuItemList.size();
        if(a.size() == 0)
            return false;
        menuItemList.remove(a.get(0));
        for(int i=0; i<menuItemList.size(); i++) {
            if(menuItemList.get(i).getName().equals(a.get(0).getName()))
            {
                menuItemList.remove(i);
                i--;
            }
            menuItemList.get(i).setId(i);
        }
        assert wellFormed() : "There is some corrupted data after deleteProduct operation";
        assert size != menuItemList.size()+1 : "The delete operation didn't perform as expected";
        return true;
    }

    /**
     * @precondition Check if there are any corrupted data in the structures before the operation
     * @postcondition Check if there are any corrupted data in the structures after the operation
     * @param name the name
     * @param list the list
     * @return
     */
    @Override
    public boolean createComposite(String name, ArrayList<BaseProduct> list) {
        assert wellFormed() : "There is data that may be corrupted before createComposite operation";
        assert name.equals("") : "Can't create composite product without a name";
        assert list.size() == 0 : "Can;t create a composite product without at least 1 product";
        int size = menuItemList.size();
        CompositeProduct compositeProduct = new CompositeProduct(name,list);
        menuItemList.add(compositeProduct);
        compositeProduct.setId(++nrProducts);
        assert wellFormed() : "There is some corrupted data after createComposite operation";
        assert size != menuItemList.size()-1 : "The createComposition method did not performed as expected";
        return false;
    }

    /**
     *
     * @param minT the min t
     * @param maxT the max t
     * @return
     */
    @Override
    public List<Order> createReport1(int minT, int maxT) {
        Map<Order,ArrayList<MenuItem>> result = orders.entrySet().stream().filter(map -> map.getKey().getDate().getHours() >= minT).filter(map -> map.getKey().getDate().getHours() <= maxT).collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
        List<Order> listResult = new ArrayList<>(result.keySet());
        return listResult;
    }

    /**
     *
     * @param nrTimesOrdered the nr times ordered
     * @return
     */
    @Override
    public List<MenuItem> createReport2(int nrTimesOrdered) {
        return menuItemList.stream().filter(product -> product.getNrTimesOrdered() >= nrTimesOrdered).collect(Collectors.toList());
    }

    /**
     *
     * @param min      the min
     * @param minValue the min value
     * @return
     */
    @Override
    public List<User> createReport3(int min, int minValue) {
        List<User> result = users.stream().filter(e -> e.getRole().equals("Client")).filter(e -> e.getNrTimesOrdered() >= min).collect(Collectors.toList());
        List<User> finalResult = new ArrayList<>();

        for(User i : result)
        {
            Map<Order, ArrayList<MenuItem>> intermediate = orders.entrySet().stream().filter(a -> a.getKey().getClientId() == i.getId()).filter(a -> a.getKey().getTotalPrice() >= minValue).collect(Collectors.toMap(a -> a.getKey(), a -> a.getValue()));
            if(intermediate.size() > 0)
                finalResult.add(i);
        }
        return finalResult;
    }

    /**
     *
     * @param day the day
     * @return
     */
    @Override
    public List<MenuItem> createReport4(int day) {
        Map<Order,ArrayList<MenuItem>> result = orders.entrySet().stream().filter(map -> map.getKey().getDate().getDay() == day).collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
        List<MenuItem> finalResult = new ArrayList<>();
        for(Order i : result.keySet())
            finalResult.addAll(result.get(i));
        finalResult = finalResult.stream().distinct().collect(Collectors.toList());
        return finalResult;
    }

    /**
     *
     * @param user the user
     * @param list the list
     */
    @Override
    public void createOrder(String user, ArrayList<MenuItem> list) {
        assert wellFormed() : "There is data that may be corrupted before createOrder operation";
        assert user.equals("") : "Can't create an order without a user to place it";
        assert list.size() == 0 : "Can't create an order without at least 1 product";
        int size = orders.size();
        ArrayList<MenuItem> finalItemsInOrder = new ArrayList<>(list);
        User userToFind = users.stream().filter(e -> e.getName().equals(user)).collect(Collectors.toList()).get(0);
        userToFind.setNrTimesOrdered(userToFind.getNrTimesOrdered()+1);
        Order order = new Order(++nrOrders,userToFind.getId(),Calendar.getInstance().getTime());
        orders.put(order,finalItemsInOrder);
        String productsInCurrentOrder="";
        for(MenuItem i : finalItemsInOrder)
        {
            order.setTotalPrice(order.getTotalPrice()+i.computePrice());
            i.setNrTimesOrdered(i.getNrTimesOrdered()+1);
            productsInCurrentOrder = productsInCurrentOrder+" "+i.getName();
        }
        setChanged();
        notifyObservers("\nOrder with id:"+order.getOrderId()+"has "+productsInCurrentOrder);
        new Bill(finalItemsInOrder,order);
        assert wellFormed() : "There is some corrupted data after createOrder operation";
        assert orders.size() != size+1 : "The createOrder method did not perform as expected";
    }

    /**
     *
     * @param name     the name
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     * @return
     */
    @Override
    public List<MenuItem> clientSearch(String name, float rating, int calories, int protein, int fat, int sodium, int price) {
        if(name.equals(""))
            return menuItemList.stream().filter(product -> product.getRating() >= rating).filter(product -> product.getCalories() <= calories).filter(product -> product.getFat() <= fat).filter(product -> product.getSodium() <= sodium).filter(product -> product.getProtein() >= protein).filter(product -> product.computePrice() <= price).collect(Collectors.toList());
        else
            return menuItemList.stream().filter(product -> product.getName().contains(name)).filter(product -> product.getRating() >= rating).filter(product -> product.getCalories() <= calories).filter(product -> product.getFat() <= fat).filter(product -> product.getSodium() <= sodium).filter(product -> product.getProtein() >= protein).filter(product -> product.computePrice() <= price).collect(Collectors.toList());
    }

    /**
     * Import menu.
     */
    public void importMenu()
    {
        var filePath = System.getProperty("user.dir")+"/products.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
        {
            List<String> lines;
            lines = br.lines().skip(1).collect(Collectors.toList());
            lines = lines.stream().distinct().collect(Collectors.toList());

            for(String i : lines)
            {
                String[] line = i.split(",",0);
                List<MenuItem> a = menuItemList.stream().filter(e -> e.getName().equals(line[0])).collect(Collectors.toList());
                if(a.size() == 0) {
                    nrProducts++;
                    MenuItem product = new BaseProduct(nrProducts, line[0], Float.parseFloat(line[1]), Integer.parseInt(line[2]), Integer.parseInt(line[3]), Integer.parseInt(line[4]), Integer.parseInt(line[5]), Integer.parseInt(line[6]));
                    menuItemList.add(product);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Well formed boolean.
     *
     * @return the boolean
     */
    public Boolean wellFormed()
    {
        for(Order j : orders.keySet())
            if(j.getOrderId() < 0 || j.getClientId() < 0 || j.getTotalPrice() <= 0)
                return false;
        for(MenuItem i : menuItemList)
            if(i.computePrice() < 0 || i.getCalories() <0 || i.getProtein() <0 || i.getSodium() <0 || i.getFat() < 0 || i.getRating() <0)
                return false;
        for(User user : users)
            if(user.getName().equals("") || user.getRole().equals("") || user.getPassword().equals(""))
                return false;
        return true;
    }

    /**
     * Gets menu item list.
     *
     * @return the menu item list
     */
    public List<MenuItem> getMenuItemList() {
        return menuItemList;
    }

    /**
     * Gets product.
     *
     * @param name the name
     * @return the product
     */
    public MenuItem getProduct(String name)
    {
        return  menuItemList.stream().filter(e -> e.getName().equals(name)).collect(Collectors.toList()).get(0);
    }
}
