package BusinessLayer;

import java.io.Serializable;

/**
 * The type Base product.
 */
public class BaseProduct implements MenuItem, Serializable {
    private String name;
    private int id;
    private float rating;
    private int calories;
    private int protein;
    private int fat;
    private int sodium;
    private int price;
    private int timesOrdered;

    /**
     * Instantiates a new Base product.
     *
     * @param id       the id
     * @param name     the name
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     */
    public BaseProduct(int id, String name, float rating, int calories, int protein, int fat, int sodium, int price)
    {
        this.id=id;
        this.name=name;
        this.rating=rating;
        this.calories=calories;
        this.protein=protein;
        this.fat=fat;
        this.sodium=sodium;
        this.price=price;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public float getRating() {
        return this.rating;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getFat() {
        return this.fat;
    }

    @Override
    public int getCalories() {
        return this.calories;
    }

    @Override
    public int getProtein() {
        return this.protein;
    }

    @Override
    public int getSodium() {
        return this.sodium;
    }

    @Override
    public int computePrice() {
        return this.price;
    }

    @Override
    public void setId(int id) {
        this.id=id;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setNrTimesOrdered(int timesOrdered) {
        this.timesOrdered = timesOrdered;
    }
    public int getNrTimesOrdered()
    {
        return timesOrdered;
    }

    @Override
    public boolean equals(Object obj)
    {
        BaseProduct aux = (BaseProduct)obj;
        return !aux.getName().equals(this.name);
    }
}
