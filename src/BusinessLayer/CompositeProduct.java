package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Composite product.
 */
public class CompositeProduct implements MenuItem, Serializable {
    /**
     * The Products.
     */
    ArrayList<BaseProduct> products;
    private String name;
    private int id;
    private int timesOrdered;
    private float rating;
    private int calories;
    private int protein;
    private int fat;
    private int sodium;
    private int price;

    /**
     * Instantiates a new Composite product.
     *
     * @param name the name
     * @param list the list
     */
    public CompositeProduct(String name, ArrayList<BaseProduct> list)
    {
        this.products=list;
        this.name=name;
    }

    /**
     * Add product.
     *
     * @param product the product
     */
    public void addProduct(BaseProduct product)
    {
        products.add(product);
    }
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public float getRating() {
        float aux = 0;
        for(int i=0; i<products.size();i++)
            aux+= products.get(i).getRating();
        return aux/products.size();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getFat() {
        int aux = 0;
        for(int i=0; i<products.size();i++)
            aux+= products.get(i).getFat();
        return aux;
    }

    @Override
    public int getCalories() {
        int aux = 0;
        for(int i=0; i<products.size();i++)
            aux+= products.get(i).getCalories();
        return aux;
    }

    @Override
    public int getProtein() {
        int aux = 0;
        for(int i=0; i<products.size();i++)
            aux+= products.get(i).getProtein();
        return aux;
    }

    @Override
    public int getSodium() {
        int aux = 0;
        for(int i=0; i<products.size();i++)
            aux+= products.get(i).getSodium();
        return aux;
    }

    @Override
    public int computePrice() {
        int aux = 0;
        for(int i=0; i<products.size();i++)
            aux+= products.get(i).computePrice();
        return aux;
    }

    @Override
    public void setId(int id) {
        this.id=id;
    }

    @Override
    public void setPrice(int price) {
        this.price=price;
    }

    @Override
    public void setRating(float rating) {
        this.rating=rating;
    }

    @Override
    public void setProtein(int protein) {
        this.protein = protein;
    }

    @Override
    public void setCalories(int calories) {
        this.calories=calories;
    }

    @Override
    public void setSodium(int sodium) {
        this.sodium=sodium;
    }

    @Override
    public void setFat(int fat) {
        this.fat=fat;
    }

    @Override
    public void setNrTimesOrdered(int nrTimesOrdered) {
        this.timesOrdered = nrTimesOrdered;
    }

    @Override
    public int getNrTimesOrdered() {
        return timesOrdered;
    }
}
