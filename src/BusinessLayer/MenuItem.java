package BusinessLayer;

/**
 * The interface Menu item.
 */
public interface MenuItem {
    /**
     * Gets id.
     *
     * @return the id
     */
    int getId();

    /**
     * Gets rating.
     *
     * @return the rating
     */
    float getRating();

    /**
     * Gets name.
     *
     * @return the name
     */
    String getName();

    /**
     * Gets fat.
     *
     * @return the fat
     */
    int getFat();

    /**
     * Gets calories.
     *
     * @return the calories
     */
    int getCalories();

    /**
     * Gets protein.
     *
     * @return the protein
     */
    int getProtein();

    /**
     * Gets sodium.
     *
     * @return the sodium
     */
    int getSodium();

    /**
     * Gets nr times ordered.
     *
     * @return the nr times ordered
     */
    int getNrTimesOrdered();

    /**
     * Compute price int.
     *
     * @return the int
     */
    int computePrice();

    /**
     * Sets id.
     *
     * @param id the id
     */
    void setId(int id);

    /**
     * Sets price.
     *
     * @param price the price
     */
    void setPrice(int price);

    /**
     * Sets rating.
     *
     * @param rating the rating
     */
    void setRating(float rating);

    /**
     * Sets protein.
     *
     * @param protein the protein
     */
    void setProtein(int protein);

    /**
     * Sets calories.
     *
     * @param calories the calories
     */
    void setCalories(int calories);

    /**
     * Sets sodium.
     *
     * @param sodium the sodium
     */
    void setSodium(int sodium);

    /**
     * Sets fat.
     *
     * @param fat the fat
     */
    void setFat(int fat);

    /**
     * Sets nr times ordered.
     *
     * @param nrTimesOrdered the nr times ordered
     */
    void setNrTimesOrdered(int nrTimesOrdered);
}
