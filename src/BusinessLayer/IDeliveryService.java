package BusinessLayer;

import Model.Order;
import Model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * The interface Delivery service.
 */
public interface IDeliveryService {
    /**
     * Add product boolean.
     *
     * @param name     the name
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     * @return the boolean
     */
    boolean addProduct(String name, float rating, int calories, int protein, int fat, int sodium, int price);

    /**
     * Modify product boolean.
     *
     * @param name     the name
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     * @return the boolean
     */
    boolean modifyProduct(String name, float rating, int calories, int protein, int fat, int sodium, int price);

    /**
     * Delete product boolean.
     *
     * @param name the name
     * @return the boolean
     */
    boolean deleteProduct(String name);

    /**
     * Create composite boolean.
     *
     * @param name the name
     * @param list the list
     * @return the boolean
     */
    boolean createComposite(String name, ArrayList<BaseProduct> list);

    /**
     * Create report 1 list.
     *
     * @param minT the min t
     * @param maxT the max t
     * @return the list
     */
    List<Order> createReport1(int minT, int maxT);

    /**
     * Create report 2 list.
     *
     * @param nrTimesOrdered the nr times ordered
     * @return the list
     */
    List<MenuItem> createReport2(int nrTimesOrdered);

    /**
     * Create report 3 list.
     *
     * @param min      the min
     * @param minValue the min value
     * @return the list
     */
    List<User> createReport3(int min, int minValue);

    /**
     * Create report 4 list.
     *
     * @param day the day
     * @return the list
     */
    List<MenuItem> createReport4(int day);

    /**
     * Create order.
     *
     * @param user the user
     * @param list the list
     */
    void createOrder(String user, ArrayList<MenuItem> list);

    /**
     * Client search list.
     *
     * @param name     the name
     * @param rating   the rating
     * @param calories the calories
     * @param protein  the protein
     * @param fat      the fat
     * @param sodium   the sodium
     * @param price    the price
     * @return the list
     */
    List<MenuItem> clientSearch(String name, float rating, int calories, int protein, int fat, int sodium, int price);
}
