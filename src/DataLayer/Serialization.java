package DataLayer;

import BusinessLayer.DeliveryService;

import java.io.*;

/**
 * The type Serialization.
 */
public class Serialization implements Serializable {
    /**
     * Instantiates a new Serialization.
     */
    public Serialization()
    { }

    /**
     * Serialize.
     *
     * @param deliveryService the delivery service
     * @param name            the name
     */
    public void serialize(DeliveryService deliveryService, String name) {
        try {
            FileOutputStream file = new FileOutputStream(name);
            ObjectOutputStream write = new ObjectOutputStream(file);

            write.writeObject(deliveryService);
            write.close();
            file.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Deserialize delivery service.
     *
     * @param name the name
     * @return the delivery service
     */
    public DeliveryService deserialize(String name) {
        DeliveryService deliveryService = null;
        try {
            FileInputStream file = new FileInputStream(name);
            if (file.getChannel().size() != 0) {
                ObjectInputStream read = new ObjectInputStream(file);
                deliveryService = (DeliveryService)read.readObject();
                read.close();
                file.close();
            }
            else
            {
                deliveryService = new DeliveryService();
            }
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return deliveryService;
    }
}
