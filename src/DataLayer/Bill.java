package DataLayer;

import BusinessLayer.MenuItem;
import Model.Order;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * The type Bill.
 */
public class Bill implements Serializable {
    private static int nrBill;

    /**
     * Instantiates a new Bill.
     *
     * @param products the products
     * @param order    the order
     */
    public Bill(ArrayList<MenuItem> products, Order order)
    {
        nrBill++;
        try {
            int total = 0;
            ///File file = new File("bill" + (nrBill) + ".txt");
            FileWriter fileWriter = new FileWriter("bill" + nrBill + ".txt");

            fileWriter.write("Bill nr."+nrBill+"\n");
            fileWriter.write("Order id."+order.getOrderId()+"\n");
            fileWriter.write("Date: "+order.getDate()+"\n");
            fileWriter.write("Client id."+order.getClientId()+"\n");
            fileWriter.write("Products\n");
            for(MenuItem i : products) {
                fileWriter.write(i.getName() + "......." + i.computePrice() + "Euro\n");
                total+=i.computePrice();
            }
            fileWriter.write("Total: "+total+" Euro");
            fileWriter.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
