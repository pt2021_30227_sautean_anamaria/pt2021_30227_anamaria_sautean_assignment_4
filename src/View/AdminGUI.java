package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * The type Admin gui.
 */
public class AdminGUI {
    private final JFrame frame = new JFrame("Admin");

    private JButton importButton = new JButton("Import");
    private JButton createButton = new JButton("Create");
    private JButton addButton = new JButton("Add");
    private JButton deleteButton = new JButton("Delete");
    private JButton modifyButton = new JButton("Modify");
    private JButton backButton = new JButton("Back");
    private JButton reportButton = new JButton("Create Report");
    private JButton checkMenuButton = new JButton("Check Menu");

    private JTextField nameField = new JTextField("",10);
    private JTextField priceField = new JTextField("",10);
    private JTextField proteinField = new JTextField("",10);
    private JTextField fatField = new JTextField("",10);
    private JTextField caloriesField = new JTextField("",10);
    private JTextField ratingField = new JTextField("",10);
    private JTextField sodiumField = new JTextField("",10);

    private JTextField timeReportMinField = new JTextField("",10);
    private JTextField timeReportMaxField = new JTextField("",10);
    private JTextField nrTimesReportField = new JTextField("",10);
    private JTextField clientReportMinField = new JTextField("",10);
    private JTextField clientReportMaxField = new JTextField("",10);
    private JTextField productReportField = new JTextField("",10);

    /**
     * Instantiates a new Admin gui.
     */
    public AdminGUI()
    {
        frame.setMinimumSize(new Dimension(800,800));
        frame.setLocationRelativeTo(null);
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        mainPanel.setLayout(new GridLayout(2,1));

        JPanel upPanel = new JPanel();
        upPanel.setLayout(new GridLayout(1,2));

        JPanel upLeftPanel = new JPanel();
        upLeftPanel.setLayout(new GridLayout(7,2));
        upLeftPanel.add(new JLabel("Title"));
        upLeftPanel.add(nameField);
        upLeftPanel.add(new JLabel("Price"));
        upLeftPanel.add(priceField);
        upLeftPanel.add(new JLabel("Rating"));
        upLeftPanel.add(ratingField);
        upLeftPanel.add(new JLabel("Fat"));
        upLeftPanel.add(fatField);
        upLeftPanel.add(new JLabel("Sodium"));
        upLeftPanel.add(sodiumField);
        upLeftPanel.add(new JLabel("Protein"));
        upLeftPanel.add(proteinField);
        upLeftPanel.add(new JLabel("Calories"));
        upLeftPanel.add(caloriesField);

        JPanel upRightPanel = new JPanel();
        upRightPanel.setLayout(new GridLayout(4,2));

        JPanel timePanel = new JPanel();
        timePanel.setLayout(new GridBagLayout());
        timePanel.add(new JLabel("Time report"));
        timePanel.add(timeReportMinField);
        timePanel.add(timeReportMaxField);

        JPanel nrTimesPanel = new JPanel();
        nrTimesPanel.setLayout(new GridBagLayout());
        nrTimesPanel.add(new JLabel("Nr. times report"));
        nrTimesPanel.add(nrTimesReportField);

        JPanel clientReport = new JPanel();
        clientReport.setLayout(new GridBagLayout());
        clientReport.add(new JLabel("Client report"));
        clientReport.add(clientReportMinField);
        clientReport.add(clientReportMaxField);

        JPanel productDayPanel = new JPanel();
        productDayPanel.setLayout(new GridBagLayout());
        productDayPanel.add(new JLabel("Product on day report"));
        productDayPanel.add(productReportField);

        upRightPanel.add(timePanel);
        upRightPanel.add(nrTimesPanel);
        upRightPanel.add(clientReport);
        upRightPanel.add(productDayPanel);

        upPanel.add(upLeftPanel);
        upPanel.add(upRightPanel);

        JPanel downPanel = new JPanel();
        downPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        downPanel.setLayout(new GridLayout(3,3));
        downPanel.add(importButton);
        downPanel.add(createButton);
        downPanel.add(modifyButton);
        downPanel.add(deleteButton);
        downPanel.add(addButton);
        downPanel.add(checkMenuButton);
        downPanel.add(reportButton);
        downPanel.add(backButton);
        mainPanel.add(upPanel);
        mainPanel.add(downPanel);
        frame.setContentPane(mainPanel);
        frame.setVisible(true);
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName()
    {
        return nameField.getText();
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public String getPrice()
    {
        return priceField.getText();
    }

    /**
     * Gets rating.
     *
     * @return the rating
     */
    public String getRating()
    {
        return ratingField.getText();
    }

    /**
     * Gets calories.
     *
     * @return the calories
     */
    public String getCalories()
    {
        return caloriesField.getText();
    }

    /**
     * Gets protein.
     *
     * @return the protein
     */
    public String getProtein()
    {
        return proteinField.getText();
    }

    /**
     * Gets sodium.
     *
     * @return the sodium
     */
    public String getSodium()
    {
        return sodiumField.getText();
    }

    /**
     * Gets fat.
     *
     * @return the fat
     */
    public String getFat()
    {
        return fatField.getText();
    }

    /**
     * Gets min time.
     *
     * @return the min time
     */
    public String getMinTime()
    {
        return timeReportMinField.getText();
    }

    /**
     * Gets max time.
     *
     * @return the max time
     */
    public String getMaxTime()
    {
        return timeReportMaxField.getText();
    }

    /**
     * Gets nr times.
     *
     * @return the nr times
     */
    public String getNrTimes()
    {
        return nrTimesReportField.getText();
    }

    /**
     * Gets client min.
     *
     * @return the client min
     */
    public String getClientMin()
    {
        return clientReportMinField.getText();
    }

    /**
     * Gets client amount.
     *
     * @return the client amount
     */
    public String getClientAmount()
    {
        return clientReportMaxField.getText();
    }

    /**
     * Day product string.
     *
     * @return the string
     */
    public String dayProduct()
    {
        return productReportField.getText();
    }

    /**
     * Gets frame.
     *
     * @return the frame
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * Add back listener.
     *
     * @param a the a
     */
    public void addBackListener(ActionListener a)
    {
        backButton.addActionListener(a);
    }

    /**
     * Add import listener.
     *
     * @param a the a
     */
    public void addImportListener(ActionListener a)
    {
        importButton.addActionListener(a);
    }

    /**
     * Add check menu.
     *
     * @param a the a
     */
    public void addCheckMenu(ActionListener a)
    {
        checkMenuButton.addActionListener(a);
    }

    /**
     * Add add listener.
     *
     * @param a the a
     */
    public void addAddListener(ActionListener a)
    {
        addButton.addActionListener(a);
    }

    /**
     * Add delete.
     *
     * @param a the a
     */
    public void addDelete(ActionListener a)
    {
        deleteButton.addActionListener(a);
    }

    /**
     * Add modify.
     *
     * @param a the a
     */
    public void addModify(ActionListener a)
    {
        modifyButton.addActionListener(a);
    }

    /**
     * Add composite.
     *
     * @param a the a
     */
    public void addComposite(ActionListener a)
    {
        createButton.addActionListener(a);
    }

    /**
     * Add report.
     *
     * @param a the a
     */
    public void addReport(ActionListener a)
    {
        reportButton.addActionListener(a);
    }

    /**
     * Sets fields null.
     */
    public void setFieldsNull()
    {
        timeReportMaxField.setText("");
        timeReportMinField.setText("");
        nrTimesReportField.setText("");
        clientReportMinField.setText("");
        clientReportMaxField.setText("");
        productReportField.setText("");
    }
}
