package View;

import BusinessLayer.DeliveryService;
import DataLayer.Serialization;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The type Log in controller.
 */
public class LogInController {
    private final LogInGUI logInGUI;
    private final DeliveryService deliveryService;

    /**
     * Instantiates a new Log in controller.
     *
     * @param logInGUI the log in gui
     */
    public LogInController(LogInGUI logInGUI) {
        this.logInGUI = logInGUI;
        this.logInGUI.addRegisterListener(new RegisterListener());
        this.logInGUI.addLogInListener(new LogInListener());
        Serialization serialization = new Serialization();
       deliveryService = serialization.deserialize("serialization.txt");
       // deliveryService = new DeliveryService();
        logInGUI.getFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                serialization.serialize(deliveryService,"serialization.txt");
            }
        });
    }

    /**
     * The type Register listener.
     */
    class RegisterListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String role = logInGUI.getRole();
            String username = logInGUI.getUsername();
            String password = logInGUI.getPassword();
            if(username.equals("") || password.equals("") || !deliveryService.addUser(username,password,role))
            {
                JFrame errorFrame = new JFrame("Error");
                errorFrame.setMinimumSize(new Dimension(300,200));
                errorFrame.setLocationRelativeTo(null);
                errorFrame.getContentPane().setLayout(new GridBagLayout());
                errorFrame.getContentPane().add(new JLabel("User already exists"));
                JButton okButton = new JButton("Ok");
                okButton.addActionListener(e1 -> errorFrame.dispose());
                errorFrame.getContentPane().add(okButton);
                errorFrame.setVisible(true);
            }
        }
    }

    /**
     * The type Log in listener.
     */
    class LogInListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String role = logInGUI.getRole();
            String username = logInGUI.getUsername();
            String password = logInGUI.getPassword();
            if(username.equals("") || password.equals("") || !deliveryService.findUser(username,password,role))
            {
                JFrame errorFrame = new JFrame("Error");
                errorFrame.setMinimumSize(new Dimension(300,200));
                errorFrame.setLocationRelativeTo(null);
                errorFrame.getContentPane().setLayout(new GridBagLayout());
                errorFrame.getContentPane().add(new JLabel("User does not exists"));
                JButton okButton = new JButton("Ok");
                okButton.addActionListener(e1 -> errorFrame.dispose());
                errorFrame.getContentPane().add(okButton);
                errorFrame.setVisible(true);
            }
            else
            {
                if(role.equals("Admin"))
                    new AdminController(new AdminGUI(),deliveryService);
                else
                {
                    if(role.equals("Client"))
                        new ClientController(new ClientGUI(),deliveryService,username);
                    else {
                        deliveryService.addObserver(new EmployeeController(new EmployeeGUI()));
                    }
                }
            }
        }
    }

}
