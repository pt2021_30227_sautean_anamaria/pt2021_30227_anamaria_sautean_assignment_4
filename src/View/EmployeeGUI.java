package View;

import javax.swing.*;
import java.awt.*;

/**
 * The type Employee gui.
 */
public class EmployeeGUI {
    private final JFrame frame = new JFrame("Employee");

    private JLabel ordersLabel = new JLabel("");
    /**
     * The Down panel.
     */
    JPanel downPanel = new JPanel();

    /**
     * Instantiates a new Employee gui.
     */
    public EmployeeGUI()
    {
        frame.setMinimumSize(new Dimension(400,400));
        frame.setLocationRelativeTo(null);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2,1));

        JPanel upPanel = new JPanel();
        upPanel.add(new JLabel("Pending orders"));

        downPanel.add(ordersLabel);

        mainPanel.add(upPanel);
        mainPanel.add(downPanel);

        frame.setContentPane(mainPanel);
        frame.setVisible(true);
    }

    /**
     * Add pending orders.
     *
     * @param order the order
     */
    public void addPendingOrders(String order)
    {
        String aux = ordersLabel.getText()+"\n"+order+"\n";
        System.out.println(aux);
        downPanel.removeAll();
        ordersLabel = new JLabel(aux);
        downPanel.add(ordersLabel);
        frame.setVisible(true);
    }
}
