package View;

import BusinessLayer.DeliveryService;

import java.util.Observable;
import java.util.Observer;

/**
 * The type Employee controller.
 */
public class EmployeeController implements Observer {
    private final EmployeeGUI employeeGUI;

    /**
     * Instantiates a new Employee controller.
     *
     * @param employeeGUI the employee gui
     */
    public EmployeeController(EmployeeGUI employeeGUI)
    {
        this.employeeGUI=employeeGUI;
    }

    @Override
    public void update(Observable o, Object arg) {
        String order = (String)arg;
        employeeGUI.addPendingOrders(order);
    }
}
