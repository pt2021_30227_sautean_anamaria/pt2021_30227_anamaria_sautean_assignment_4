package View;

import BusinessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * The type Client gui.
 */
public class ClientGUI {
    private final JFrame frame = new JFrame("Client");

    private JTextField searchTitleField = new JTextField("",10);
    private JTextField searchPriceField = new JTextField("",10);
    private JTextField searchRatingField = new JTextField("",10);
    private JTextField searchCaloriesField = new JTextField("",10);
    private JTextField searchProteinField = new JTextField("",10);
    private JTextField searchFatField = new JTextField("",10);
    private JTextField searchSodiumField = new JTextField("",10);

    private JButton searchButton = new JButton("Search");
    private JButton addButton = new JButton("Add");
    private JButton submitButton = new JButton("Submit");
    private JButton checkMenuButton = new JButton("Check Menu");

    private JComboBox orderProducts = new JComboBox();

    /**
     * Instantiates a new Client gui.
     */
    public ClientGUI()
    {
        frame.setMinimumSize(new Dimension(500,500));
        frame.setLocationRelativeTo(null);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2,1));

        JPanel upPanel = new JPanel();
        upPanel.setLayout(new GridLayout(8,2));
        upPanel.add(new JLabel("Title"));
        upPanel.add(searchTitleField);
        upPanel.add(new JLabel("Price"));
        upPanel.add(searchPriceField);
        upPanel.add(new JLabel("Rating"));
        upPanel.add(searchRatingField);
        upPanel.add(new JLabel("Calories"));
        upPanel.add(searchCaloriesField);
        upPanel.add(new JLabel("Protein"));
        upPanel.add(searchProteinField);
        upPanel.add(new JLabel("Fat"));
        upPanel.add(searchFatField);
        upPanel.add(new JLabel("Sodium"));
        upPanel.add(searchSodiumField);
        upPanel.add(searchButton);

        JPanel downPanel = new JPanel();
        downPanel.setLayout(new GridLayout(5,1));
        downPanel.add(new JLabel("Perform orders"));
        downPanel.add(orderProducts);
        downPanel.add(addButton);
        downPanel.add(submitButton);
        downPanel.add(checkMenuButton);

        mainPanel.add(upPanel);
        mainPanel.add(downPanel);
        frame.setContentPane(mainPanel);
        frame.setVisible(true);
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return searchTitleField.getText();
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public String getPrice()
    {
        return searchPriceField.getText();
    }

    /**
     * Gets rating.
     *
     * @return the rating
     */
    public String getRating()
    {
        return searchRatingField.getText();
    }

    /**
     * Gets calories.
     *
     * @return the calories
     */
    public String getCalories()
    {
        return searchCaloriesField.getText();
    }

    /**
     * Gets protein.
     *
     * @return the protein
     */
    public String getProtein()
    {
        return searchProteinField.getText();
    }

    /**
     * Gets fat.
     *
     * @return the fat
     */
    public String getFat()
    {
        return searchFatField.getText();
    }

    /**
     * Gets sodium.
     *
     * @return the sodium
     */
    public String getSodium()
    {
        return searchSodiumField.getText();
    }

    /**
     * Add products.
     *
     * @param list the list
     */
    public void addProducts(ArrayList<BusinessLayer.MenuItem> list)
    {
        for(MenuItem i : list)
            orderProducts.addItem(i.getName());
        frame.setVisible(true);
    }

    /**
     * Add add listener.
     *
     * @param a the a
     */
    public void addAddListener(ActionListener a)
    {
        addButton.addActionListener(a);
    }

    /**
     * Add submit listener.
     *
     * @param a the a
     */
    public void addSubmitListener(ActionListener a) {
        submitButton.addActionListener(a);
    }

    /**
     * Add check menu listener.
     *
     * @param a the a
     */
    public void addCheckMenuListener(ActionListener a)
    {
        checkMenuButton.addActionListener(a);
    }

    /**
     * Add search listener.
     *
     * @param a the a
     */
    public void addSearchListener(ActionListener a)
    {
        searchButton.addActionListener(a);
    }

    /**
     * Gets selected item.
     *
     * @return the selected item
     */
    public String getSelectedItem()
    {
        return (String) orderProducts.getSelectedItem();
    }
}
