package View;

import BusinessLayer.DeliveryService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * The type Log in gui.
 */
public class LogInGUI {
    private final JFrame frame = new JFrame("LogIn");
    private final JButton registerButton = new JButton("Register");
    private final JButton logInButton = new JButton("Log In");
    private final JTextField usernameText = new JTextField("",20);
    private final JTextField passwordText = new JTextField("",20);
    private final JRadioButton adminButton = new JRadioButton("Admin");
    private final JRadioButton employeeButton = new JRadioButton("Employee");

    /**
     * Instantiates a new Log in gui.
     */
    public LogInGUI()
    {
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(600,400));
        frame.getContentPane().setLayout(new GridLayout(4,1,2,5));

        JPanel username = new JPanel();
        JPanel role = new JPanel();
        JPanel password = new JPanel();
        JPanel buttons = new JPanel();

        username.setLayout(new GridBagLayout());
        username.add(new JLabel("Username"));
        username.add(usernameText);

        password.setLayout(new GridBagLayout());
        password.add(new JLabel("Password"));
        password.add(passwordText);

        ButtonGroup roleGroup = new ButtonGroup();
        roleGroup.add(adminButton);
        roleGroup.add(employeeButton);
        JRadioButton clientButton = new JRadioButton("Client");
        roleGroup.add(clientButton);

        role.add(adminButton);
        role.add(employeeButton);
        role.add(clientButton);

        buttons.add(registerButton);
        buttons.add(logInButton);

        frame.getContentPane().add(username);
        frame.getContentPane().add(password);
        frame.getContentPane().add(role);
        frame.getContentPane().add(buttons);
        frame.setVisible(true);
    }

    /**
     * Add register listener.
     *
     * @param e the e
     */
    public void addRegisterListener(ActionListener e)
    {
        registerButton.addActionListener(e);
    }

    /**
     * Add log in listener.
     *
     * @param e the e
     */
    public void addLogInListener(ActionListener e)
    {
        logInButton.addActionListener(e);
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public String getRole()
    {
        if(adminButton.isSelected())
            return "Admin";
        if(employeeButton.isSelected())
            return "Employee";
        return "Client";
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername()
    {
        return usernameText.getText();
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword()
    {
        return passwordText.getText();
    }

    /**
     * Gets frame.
     *
     * @return the frame
     */
    public JFrame getFrame() {
        return frame;
    }

}
