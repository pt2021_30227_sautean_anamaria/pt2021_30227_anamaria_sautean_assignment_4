package View;

import BusinessLayer.DeliveryService;
import BusinessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Client controller.
 */
public class ClientController {
    private ClientGUI clientGUI;
    private DeliveryService deliveryService;
    private List<MenuItem> productsInOrder = new ArrayList<>();
    private String user;

    /**
     * Instantiates a new Client controller.
     *
     * @param clientGUI       the client gui
     * @param deliveryService the delivery service
     * @param user            the user
     */
    public ClientController(ClientGUI clientGUI, DeliveryService deliveryService, String user)
    {
        this.clientGUI=clientGUI;
        this.deliveryService=deliveryService;
        this.user=user;
        clientGUI.addProducts((ArrayList<MenuItem>) deliveryService.getMenuItemList());
        clientGUI.addAddListener(new AddListener());
        clientGUI.addSubmitListener(new SubmitListener());
        clientGUI.addCheckMenuListener(new CheckMenu());
        clientGUI.addSearchListener(new SearchListener());
    }

    /**
     * The type Submit listener.
     */
    class SubmitListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            deliveryService.createOrder(user, (ArrayList<MenuItem>) productsInOrder);
            productsInOrder.clear();
            JFrame messageFrame = new JFrame("Confirmation");
            messageFrame.setMinimumSize(new Dimension(400,200));
            messageFrame.setLocationRelativeTo(null);
            messageFrame.setLayout(new GridLayout(2,1));
            JPanel labelPanel = new JPanel();
            labelPanel.add(new JLabel("Your order was successfully placed"));
            messageFrame.add(labelPanel);

            JPanel okPanel = new JPanel();
            JButton okButton = new JButton("Ok");
            okButton.addActionListener(e1 -> messageFrame.dispose());
            okPanel.add(okButton);

            messageFrame.add(okPanel);
            messageFrame.setVisible(true);
        }
    }

    /**
     * The type Add listener.
     */
    class AddListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String productOrdered = clientGUI.getSelectedItem();
            MenuItem result = deliveryService.getProduct(productOrdered);
            productsInOrder.add(result);
        }
    }

    /**
     * The type Check menu.
     */
    class CheckMenu implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            JFrame menuFrame = new JFrame("Menu");
            String[] tableNames = {"Id","Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
            JScrollPane scroll;
            menuFrame.setMinimumSize(new Dimension(1000,300));
            menuFrame.setLocationRelativeTo(null);

            List<MenuItem> list = deliveryService.getMenuItemList();
            Object[][] product = new Object[list.size()][8];
            int counter = 0;
            for(MenuItem i : list)
            {
                product[counter][0] = i.getId();
                product[counter][1] = i.getName();
                product[counter][2] = i.getRating();
                product[counter][3] = i.getCalories();
                product[counter][4] = i.getProtein();
                product[counter][5] = i.getFat();
                product[counter][6] = i.getSodium();
                product[counter][7] = i.computePrice();
                counter++;
            }
            JTable table = new JTable(product,tableNames);
            table.setFillsViewportHeight(true);
            table.setPreferredScrollableViewportSize(new Dimension(500,500));
            scroll = new JScrollPane(table);

            menuFrame.add(scroll);
            menuFrame.setVisible(true);
        }
    }

    /**
     * The type Search listener.
     */
    class SearchListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String name;
            float rating;
            int calories;
            int protein;
            int fat;
            int sodium;
            int price;

            if(clientGUI.getTitle().equals(""))
                name="";
            else
                name=clientGUI.getTitle();

            if(clientGUI.getRating().equals(""))
                rating=0;
            else
                rating=Float.parseFloat(clientGUI.getRating());

            if(clientGUI.getCalories().equals(""))
                calories=Integer.MAX_VALUE;
            else
                calories=Integer.parseInt(clientGUI.getCalories());

            if(clientGUI.getProtein().equals(""))
                protein=0;
            else
                protein=Integer.parseInt(clientGUI.getProtein());

            if(clientGUI.getFat().equals(""))
                fat= Integer.MAX_VALUE;
            else
                fat=Integer.parseInt(clientGUI.getFat());

            if(clientGUI.getSodium().equals(""))
                sodium= Integer.MAX_VALUE;
            else
                sodium=Integer.parseInt(clientGUI.getSodium());

            if(clientGUI.getPrice().equals(""))
                price= Integer.MAX_VALUE;
            else
                price = Integer.parseInt(clientGUI.getPrice());

            List<MenuItem> result = deliveryService.clientSearch(name,rating,calories,protein,fat,sodium,price);
            String[] names = {"Id","Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
            JFrame frame = new JFrame("Menu");
            JScrollPane scroll;
            frame.setMinimumSize(new Dimension(1000,300));
            frame.setLocationRelativeTo(null);

            Object[][] product = new Object[result.size()][8];
            int counter = 0;
            for(MenuItem i : result)
            {
                product[counter][0] = i.getId();
                product[counter][1] = i.getName();
                product[counter][2] = i.getRating();
                product[counter][3] = i.getCalories();
                product[counter][4] = i.getProtein();
                product[counter][5] = i.getFat();
                product[counter][6] = i.getSodium();
                product[counter][7] = i.computePrice();
                counter++;
            }
            JTable table = new JTable(product,names);
            table.setFillsViewportHeight(true);
            table.setPreferredScrollableViewportSize(new Dimension(500,500));
            scroll = new JScrollPane(table);

            frame.add(scroll);
            frame.setVisible(true);
        }
    }
}
