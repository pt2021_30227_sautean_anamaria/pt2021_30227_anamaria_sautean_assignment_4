package View;

import BusinessLayer.BaseProduct;
import BusinessLayer.DeliveryService;
import BusinessLayer.MenuItem;
import Model.Order;
import Model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

/**
 * The type Admin controller.
 */
public class AdminController {
    private AdminGUI adminGUI;
    private DeliveryService deliveryService;
    private String[] tableNames = {"Id","Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
    private Object[][] data = new Object[100][100];
    private JTable table = new JTable(data, tableNames);
    private JScrollPane scroll;
    private ArrayList<BaseProduct> composite = new ArrayList<>();
    private int nrCompositeProducts;
    private String name = "";
    private String finalName ="";
    private JLabel labelNrProducts = new JLabel("Nr. products: "+nrCompositeProducts);

    /**
     * Instantiates a new Admin controller.
     *
     * @param adminGUI        the admin gui
     * @param deliveryService the delivery service
     */
    public AdminController(AdminGUI adminGUI, DeliveryService deliveryService)
    {
        this.adminGUI=adminGUI;
        this.deliveryService=deliveryService;
        adminGUI.addBackListener(new BackButton());
        adminGUI.addImportListener(new ImportButton());
        adminGUI.addCheckMenu(new CheckMenu());
        adminGUI.addAddListener(new AddButton());
        adminGUI.addDelete(new DeleteButton());
        adminGUI.addModify(new ModifyButton());
        adminGUI.addComposite(new CreateComposite());
        adminGUI.addReport(new ReportGenerator());
    }

    /**
     * The type Back button.
     */
    class BackButton implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            adminGUI.getFrame().dispose();
        }
    }

    /**
     * The type Import button.
     */
    class ImportButton implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            deliveryService.importMenu();
        }
    }

    /**
     * The type Check menu.
     */
    class CheckMenu implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            JFrame menuFrame = new JFrame("Menu");
            menuFrame.setMinimumSize(new Dimension(1000,300));
            menuFrame.setLocationRelativeTo(null);

            List<BusinessLayer.MenuItem> list = deliveryService.getMenuItemList();
            Object[][] product = new Object[list.size()][8];
            int counter = 0;
            for(MenuItem i : list)
            {
                product[counter][0] = i.getId();
                product[counter][1] = i.getName();
                product[counter][2] = i.getRating();
                product[counter][3] = i.getCalories();
                product[counter][4] = i.getProtein();
                product[counter][5] = i.getFat();
                product[counter][6] = i.getSodium();
                product[counter][7] = i.computePrice();
                counter++;
            }
            JTable table = new JTable(product,tableNames);
            table.setFillsViewportHeight(true);
            table.setPreferredScrollableViewportSize(new Dimension(500,500));
            scroll = new JScrollPane(table);

            menuFrame.add(scroll);
            menuFrame.setVisible(true);
        }
    }

    /**
     * The type Add button.
     */
    class AddButton implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String name = adminGUI.getName();
                float rating = Float.parseFloat(adminGUI.getRating());
                int calories = Integer.parseInt(adminGUI.getCalories());
                int protein = Integer.parseInt(adminGUI.getProtein());
                int fat = Integer.parseInt(adminGUI.getFat());
                int sodium = Integer.parseInt(adminGUI.getSodium());
                int price = Integer.parseInt(adminGUI.getPrice());
                if(!deliveryService.addProduct(name,rating,calories,protein,fat,sodium,price))
                    throw new Exception();
            }
            catch(Exception a)
            {
                a.printStackTrace();
                JFrame errorFrame = new JFrame("Error");
                errorFrame.setMinimumSize(new Dimension(500,500));
                errorFrame.setLocationRelativeTo(null);
                errorFrame.setLayout(new GridLayout(2,1));

                errorFrame.add(new JLabel("Error at insertion"));
                JPanel buttonPanel = new JPanel();
                JButton button = new JButton("Ok");
                button.addActionListener(e1 -> errorFrame.dispose());
                buttonPanel.add(button);

                errorFrame.add(buttonPanel);
                errorFrame.setVisible(true);
            }
        }
    }

    /**
     * The type Delete button.
     */
    class DeleteButton implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String name = adminGUI.getName();
                if(!deliveryService.deleteProduct(name))
                    throw new Exception();
            }
            catch(Exception a)
            {
                a.printStackTrace();
                JFrame errorFrame = new JFrame("Error");
                errorFrame.setMinimumSize(new Dimension(500,500));
                errorFrame.setLocationRelativeTo(null);
                errorFrame.setLayout(new GridLayout(2,1));

                errorFrame.add(new JLabel("The product does not exists"));
                JPanel buttonPanel = new JPanel();
                JButton button = new JButton("Ok");
                button.addActionListener(e1 -> errorFrame.dispose());
                buttonPanel.add(button);

                errorFrame.add(buttonPanel);
                errorFrame.setVisible(true);
            }
        }
    }

    /**
     * The type Modify button.
     */
    class ModifyButton implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            try
            {
                String name = adminGUI.getName();
                float rating = Float.parseFloat(adminGUI.getRating());
                int calories = Integer.parseInt(adminGUI.getCalories());
                int protein = Integer.parseInt(adminGUI.getProtein());
                int fat = Integer.parseInt(adminGUI.getFat());
                int sodium = Integer.parseInt(adminGUI.getSodium());
                int price = Integer.parseInt(adminGUI.getPrice());
                if(name.equals(""))
                    throw new InputMismatchException();
                if(!deliveryService.modifyProduct(name,rating,calories,protein,fat,sodium,price))
                    throw new Exception();
            }
            catch (InputMismatchException a)
            {
                a.printStackTrace();
                JFrame errorFrame = new JFrame("Error");
                errorFrame.setMinimumSize(new Dimension(500,500));
                errorFrame.setLocationRelativeTo(null);
                errorFrame.setLayout(new GridLayout(2,1));

                errorFrame.add(new JLabel("You must enter the name"));
                JPanel buttonPanel = new JPanel();
                JButton button = new JButton("Ok");
                button.addActionListener(e1 -> errorFrame.dispose());
                buttonPanel.add(button);

                errorFrame.add(buttonPanel);
                errorFrame.setVisible(true);
            }
            catch (Exception a)
            {
                a.printStackTrace();
                JFrame errorFrame = new JFrame("Error");
                errorFrame.setMinimumSize(new Dimension(500,500));
                errorFrame.setLocationRelativeTo(null);
                errorFrame.setLayout(new GridLayout(2,1));

                errorFrame.add(new JLabel("Fail to modify"));
                JPanel buttonPanel = new JPanel();
                JButton button = new JButton("Ok");
                button.addActionListener(e1 -> errorFrame.dispose());
                buttonPanel.add(button);

                errorFrame.add(buttonPanel);
                errorFrame.setVisible(true);
            }
        }
    }

    /**
     * The type Create composite.
     */
    class CreateComposite implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            JFrame frame = new JFrame("Composite");
            frame.setMinimumSize(new Dimension(500,500));
            frame.setLocationRelativeTo(null);

            JPanel mainPanel = new JPanel();
            mainPanel.setLayout(new GridLayout(3,2));

            JLabel nameLabel = new JLabel("Name");
            JTextField nameText = new JTextField();

            JComboBox comboBox = new JComboBox();
            List<MenuItem> list = deliveryService.getMenuItemList();
            for(MenuItem i : list)
                comboBox.addItem(i.getName());

            JPanel labelPanel = new JPanel();
            labelPanel.add(labelNrProducts);

            JButton addButton = new JButton("Add");
            addButton.addActionListener(e1 -> {
                nrCompositeProducts++;
                labelNrProducts.setText("Nr. products: "+nrCompositeProducts);
                frame.setVisible(true);
                name+=" "+comboBox.getSelectedItem();
                composite.add((BaseProduct) deliveryService.getProduct((String) comboBox.getSelectedItem()));
            });

            JButton submitButton = new JButton("Submit");
            submitButton.addActionListener(e12 -> {
                frame.dispose();
                finalName = nameText.getText()+" "+name;
                deliveryService.createComposite(finalName,composite);
                nrCompositeProducts=0;
                labelNrProducts.setText("Nr. products: "+nrCompositeProducts);
            });
            mainPanel.add(nameLabel);
            mainPanel.add(nameText);
            mainPanel.add(comboBox);
            mainPanel.add(addButton);
            mainPanel.add(labelNrProducts);
            mainPanel.add(submitButton);
            frame.setContentPane(mainPanel);
            frame.setVisible(true);
        }
    }

    /**
     * The type Report generator.
     */
    class ReportGenerator implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            int tMin;
            int tMax;
            int nrOrdered;
            int minOrder;
            int minValue;
            int day;
            int counter = 0;
            JFrame reportFrame = new JFrame("Report");
            reportFrame.setMinimumSize(new Dimension(500,500));
            JTable reportTable;
            JScrollPane reportScroll;
            if(!adminGUI.getMinTime().equals(""))
            {
                tMin=Integer.parseInt(adminGUI.getMinTime());
                tMax=Integer.parseInt(adminGUI.getMaxTime());
                List<Order> result = deliveryService.createReport1(tMin,tMax);
                String[] names = {"Order ID", "Client ID","Date"};
                Object[][] data = new Object[result.size()][3];
                for(Order i : result)
                {
                    data[counter][0]= i.getOrderId();
                    data[counter][1]= i.getClientId();
                    data[counter][2]= i.getDate();
                    counter++;
                }
                reportTable = new JTable(data,names);
            }
            else
            {
                if(!adminGUI.getNrTimes().equals(""))
                {
                    nrOrdered=Integer.parseInt(adminGUI.getNrTimes());
                    List<MenuItem> result = deliveryService.createReport2(nrOrdered);
                    String[] names = {"Id","Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price","TimesOrdered"};
                    Object[][] data = new Object[result.size()][9];
                    for(MenuItem i : result)
                    {
                        data[counter][0] = i.getId();
                        data[counter][1] = i.getName();
                        data[counter][2] = i.getRating();
                        data[counter][3] = i.getCalories();
                        data[counter][4] = i.getProtein();
                        data[counter][5] = i.getFat();
                        data[counter][6] = i.getSodium();
                        data[counter][7] = i.computePrice();
                        data[counter][8] = i.getNrTimesOrdered();
                        counter++;
                    }
                    reportTable = new JTable(data,names);
                }
                else
                {
                    if(!adminGUI.getClientMin().equals(""))
                    {
                        minOrder=Integer.parseInt(adminGUI.getClientMin());
                        minValue=Integer.parseInt(adminGUI.getClientAmount());
                        List<User> result = deliveryService.createReport3(minOrder,minValue);
                        String[] names = {"Client ID","Username"};
                        Object[][] data = new Object[result.size()][2];
                        for(User i: result)
                        {
                            data[counter][0] = i.getId();
                            data[counter][1] = i.getName();
                            counter++;
                        }
                        reportTable = new JTable(data,names);
                    }
                    else
                    {
                        day = Integer.parseInt(adminGUI.dayProduct());
                        List<MenuItem> result = deliveryService.createReport4(day);
                        String[] names = {"Id","Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price","TimesOrdered"};
                        Object[][] data = new Object[result.size()][9];
                        for(MenuItem i : result)
                        {
                            data[counter][0] = i.getId();
                            data[counter][1] = i.getName();
                            data[counter][2] = i.getRating();
                            data[counter][3] = i.getCalories();
                            data[counter][4] = i.getProtein();
                            data[counter][5] = i.getFat();
                            data[counter][6] = i.getSodium();
                            data[counter][7] = i.computePrice();
                            data[counter][8] = i.getNrTimesOrdered();
                            counter++;
                        }
                        reportTable = new JTable(data,names);
                    }
                }
            }
            reportScroll = new JScrollPane(reportTable);
            reportFrame.getContentPane().removeAll();
            reportFrame.getContentPane().add(reportScroll);
            reportFrame.setVisible(true);
            clearFields();
        }
    }

    /**
     * Clear fields.
     */
    public void clearFields()
    {
        adminGUI.setFieldsNull();
    }
}
